require("dotenv").config();
const util = require("util");
const mysql = require("mysql2-promise")();

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  //  try {
  //    await connect();
  //    console.log("Database connected!");
  //  } catch (e) {
  //    console.error(e);
  //  }
};

const queryProductById = async (productId) => {
  return (await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`))[0][0];
};

const queryRandomProduct = async () => {
  // This function queries a random product from the database.
  return (await mysql.query(`SELECT * FROM products
                              ORDER BY RAND()
                              LIMIT 1;`))[0][0];
};

const queryAllProducts = async () => {
  // This function queries all products from the database.
  return (await mysql.query(`SELECT *
                            FROM products;`))[0];
};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

const insertOrder = async (order) => {
  // This function inserts an order into the database.
  return await mysql.query(`INSERT into orders (id, user_id, total_amount)
                            VALUES ("${order.id}", "${order.user_id}", "${order.total_amount}");`);
};

const updateUser = async (id, updates) => {
  // This function updates a user in the database.
  let query = "UPDATE users SET ";
  let fields = [];
  if (updates.email) fields.push(`email = "${updates.email}"`);
  if (updates.password) fields.push(`password = "${updates.password}"`);
  if (updates.name) fields.push(`name = "${updates.name}"`);
  query += fields.join(", ");
  query += ` WHERE id = "${id}";`;
  return (await mysql.query(query))[0];
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};

require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  console.log("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  // This function queries a random product from the database.
  try {
    const command = new ScanCommand({
      TableName: "Products",
    });
    const response = await docClient.send(command);
    const products = response.Items;
    const randomIndex = Math.floor(Math.random() * products.length);
    return products[randomIndex];
  } catch (err) {
    console.log(err);
  }
};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {
  // This function queries all products from the database.
  // If a category is specified, it will only return products in that category.
  // If a category is not specified, it will return all products.
  try {
    const command = category === "" ? new ScanCommand({ TableName: "Products" }) : new ScanCommand({
      TableName: "Products",
      FilterExpression: "category_id = :category",
      ExpressionAttributeValues: {
        ":category": category,
      },
    });
    const response = await docClient.send(command);
    return response.Items;
  } catch (err) {
    console.log(err);
  }
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {
  // This function inserts an order into the database.
  const command = new PutCommand({
    TableName: "Orders",
    Item: {
      id: order.id,
      user_id: order.user_id,
      total_amount: order.total_amount,
    },
  });
  const response = await docClient.send(command);
  return response;
};

const updateUser = async (id, updates) => {
  // This function updates a user in the database.
  // If a field is not specified in updates, it will not be updated.
  // If a field is specified in updates, it will be updated.

  const updateExpressionParts = [];
  const expressionAttributeValues = {};
  const expressionAttributeNames = {};

  if (updates.email) {
    updateExpressionParts.push("#email = :email");
    expressionAttributeNames["#email"] = "email";
    expressionAttributeValues[":email"] = updates.email;
  }

  if (updates.password) {
    updateExpressionParts.push("#password = :password");
    expressionAttributeNames["#password"] = "password";
    expressionAttributeValues[":password"] = updates.password;
  }

  if (updates.name) {
    updateExpressionParts.push("#name = :name");
    expressionAttributeNames["#name"] = "name"; 
    expressionAttributeValues[":name"] = updates.name;
  }

  const updateExpression = `set ${updateExpressionParts.join(", ")}`;

  const command = new UpdateCommand({
    TableName: "Users",
    Key: {
      id: id,
    },
    UpdateExpression: updateExpression,
    ExpressionAttributeValues: expressionAttributeValues,
    ExpressionAttributeNames: expressionAttributeNames,
    ReturnValues: "UPDATED_NEW",
  });

  const response = await docClient.send(command);
  return response;
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};

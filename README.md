# Lab 1 Skeleton

## Getting started

You'll need to put the following in your .env (keep in mind, never commit this in normal circumstances, and you can delete it from your repo once the file is on your EC2 instance):

```
# AWS RDS

RDS_HOSTNAME=<your rds hostname>
RDS_USERNAME=<your rds username>
RDS_PASSWORD=<your rds password>
RDS_PORT=<your rds port>

# AWS DYNAMODB

AWS_ACCESS_KEY_ID=<your rds access key id>
AWS_SECRET_ACCESS_KEY=<your rds secret access key>
AWS_REGION=us-east-1
```

Initialize an EC2 instance:

Reference: [here](https://sumantmishra.medium.com/how-to-deploy-node-js-app-on-aws-with-github-db99758294f1)

Go to AWS dashboard -> EC2, then click Launch Instance.
Leave all on free tier defaults, but allow all connections. Create key-pair value.
Create.
Set security group rules (EDIT -> NEW -> ALL TRAFFIC -> ANYWHERE)
SSH in with downloaded key (Connect to Instance -> SSH Client).
`chmod 400 <KEY>.pem`

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 16
```

```
sudo yum install git -y
git config --global user.name "Your Name"
git config --global user.email "Your Email"
```

```
ssh-keygen
cd .ssh
cat id_rsa.pub
```

ADD GENERATED SSH KEY TO GITLAB, (EXPIRY IN 1 DAY OR REMOVE AFTER REPO HAS BEEN CLONED IF YOU WANT TO BE EXTRA SECURE)

GIT CLONE

```
npm i
npm start
```

To do other things while your server is running:

* ctrl+z to stop,
* `bg` to continue in background
* `jobs` to list running tasks in bg
* `fg` to bring back to foreground (it's a stack, you can learn more online)

TO CREATE A SESSION THAT WILL STAY AFTER YOU LOGOUT:

```
screen -S <session-name>
npm start
```

To detach (go back to SSH):

CTRL + A + D

To re-attach (on new SSH session):

```
screen -ls
screen -r <screen name>
```
